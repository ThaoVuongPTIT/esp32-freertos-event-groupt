/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "sdkconfig.h"
#include "input.h"
#include "output.h"
//Tao ra 1 even press
#define Button_Pree_bit (1 << 0)
#define Button_Hold_bit (1 << 1)
#define Button_Realse_bit (1 << 2)
//Tao ra 1 event groupt
EventGroupHandle_t xCreateEventGroupt; 
void Button_CB(int pin)
{
    xEventGroupSetBits(xCreateEventGroupt,Button_Pree_bit);
}

//Set bit even groupt  trong ISR 
void Button_callback(int pin)
{
    BaseType_t xHigherPriorityTaskWohon = pdFALSE;
    //set bit 0 orr bit 4 trong evant group
    xEventGroupSetBitsFromISR(xCreateEventGroupt, Button_Pree_bit, &xHigherPriorityTaskWohon);
    if(xHigherPriorityTaskWohon == pdTRUE)
    {
        portYIELD_FROM_ISR();
    }
}
void vTask1Code(void * pvParameters)
{
    while(1)
    {
        EventBits_t uBit = xEventGroupWaitBits(xCreateEventGroupt, Button_Pree_bit |Button_Hold_bit |Button_Realse_bit,
        pdTRUE, pdFALSE, 3000 /portTICK_RATE_MS );
        if(uBit & Button_Pree_bit)
        {
            output_toggle(2);
            printf("Button iss press\n");
        }
    }
}
void app_main(void)
{
    output_create(2);
    input_create(4, HIGH_TO_LOW);
    input_set_callback(Button_callback);
    xCreateEventGroupt = xEventGroupCreate();
    xTaskCreate(vTask1Code, "Task1", 2048, NULL, 4, NULL);
}
