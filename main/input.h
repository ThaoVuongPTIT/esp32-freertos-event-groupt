#ifndef __INPUT_H
#define __INPUT_H

typedef enum
{
    LOW_TO_HIGH = 1,
    HIGH_TO_LOW = 2,
    ENY_EDGE    = 3
}input_int_type_t;
/*tao ra 1 function poiter: Chu trình như sau: khi có sự kiện ngắt xẩy ra -> function poiter được gọi -> hàm
call backk ở main cũng được gọi ra chạy luôn: Ban đầu cho cái input_int_hande = NULL . nếu biến này khác roongs
thì lại nhảy tới call back. biến input_int_hande khác null tức là nó có sự kiện ngắt xẩy ra*/
typedef void (*input_int_handler_t) (int pin);
void input_set_callback(void *cb);
void input_create(int pin, input_int_type_t type);
#endif